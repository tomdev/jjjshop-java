ALTER TABLE `jjjshop_order_refund`
    MODIFY COLUMN `send_time`  timestamp NULL DEFAULT NULL COMMENT '用户发货时间' AFTER `is_user_send`,
    MODIFY COLUMN `deliver_time`  timestamp NULL DEFAULT NULL COMMENT '平台发货时间' AFTER `status`;

ALTER TABLE jjjshop_shop_opt_log
    MODIFY COLUMN `create_time`  timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间';

ALTER TABLE jjjshop_shop_login_log
    MODIFY COLUMN `create_time`  timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间';

ALTER TABLE jjjshop_product_comment
    MODIFY COLUMN `create_time`  timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    MODIFY COLUMN `update_time`  timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';

ALTER TABLE `jjjshop_order`
    MODIFY COLUMN `pay_time`  timestamp NULL DEFAULT NULL COMMENT '付款时间' AFTER `pay_status`,
    MODIFY COLUMN `pay_end_time`  timestamp NULL DEFAULT NULL COMMENT '支付截止时间' AFTER `pay_time`,
    MODIFY COLUMN `delivery_time`  timestamp NULL DEFAULT NULL COMMENT '发货时间' AFTER `delivery_status`,
    MODIFY COLUMN `receipt_time`  timestamp NULL DEFAULT NULL COMMENT '收货时间' AFTER `receipt_status`;

ALTER TABLE jjjshop_tag
    MODIFY COLUMN `create_time`  timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    MODIFY COLUMN `update_time`  timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';

ALTER TABLE `jjjshop_product`
    MODIFY COLUMN `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '产品详情' AFTER `deduct_stock_type`;

UPDATE `jjjshop_shop_access` SET `path` = '/order/operate/confirmCancel' WHERE `access_id` = 257;

INSERT INTO `jjjshop_shop_access` (`access_id`, `name`, `path`, `parent_id`, `sort`, `icon`, `redirect_name`, `is_route`, `is_menu`, `alias`, `is_show`, `plus_category_id`, `remark`, `app_id`) VALUES (1662211604, '售后审核', '/order/refund/audit', 45, 1, '', '', 0, 0, '', 1, 0, '', 10001);

INSERT INTO `jjjshop_shop_access` (`access_id`, `name`, `path`, `parent_id`, `sort`, `icon`, `redirect_name`, `is_route`, `is_menu`, `alias`, `is_show`, `plus_category_id`, `remark`, `app_id`) VALUES (1656388735, '余额明细', '/user/balance/log', 120, 1, '', '', 1, 0, '', 1, 0, '', 10001);

INSERT INTO `jjjshop_shop_access` (`access_id`, `name`, `path`, `parent_id`, `sort`, `icon`, `redirect_name`, `is_route`, `is_menu`, `alias`, `is_show`, `plus_category_id`, `remark`, `app_id`) VALUES (1656386098, '商品上下架', '/product/product/state', 15, 4, '', '', 1, 0, 'product_copy', 1, 0, '', NULL);

INSERT INTO `jjjshop_shop_access` (`access_id`, `name`, `path`, `parent_id`, `sort`, `icon`, `redirect_name`, `is_route`, `is_menu`, `alias`, `is_show`, `plus_category_id`, `remark`, `app_id`) VALUES (1628828907, '会员标签', '/user/user/tag', 55, 4, '', '', 1, 0, '', 1, 0, '', 10001);

INSERT INTO `jjjshop_shop_access` (`access_id`, `name`, `path`, `parent_id`, `sort`, `icon`, `redirect_name`, `is_route`, `is_menu`, `alias`, `is_show`, `plus_category_id`, `remark`, `app_id`) VALUES (1656064981, '升级日志', '/user/grade/log', 56, 3, '', '', 1, 0, '/member/grade/delete', 1, 0, '', NULL);

UPDATE `jjjshop_shop_access` SET `path` = '/order/operate/export' WHERE `access_id` = 1616228316;

UPDATE `jjjshop_shop_access` SET `path` = '/order/operate/extract' WHERE `access_id` = 258;